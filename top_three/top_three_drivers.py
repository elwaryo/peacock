#!/usr/bin/env python

import argparse
import csv
import operator

TOTAL_LAP_TIMES_KEY = 'total_lap_times'
LAP_COUNT_KEY = 'count'


def get_driver_name(row):
    # Driver's name must always be stripped
    # Calling this function with a value of None for the driver's name
    # value will cause an error and it's expected behavior.
    return row[0].strip().capitalize()


def get_lap_time(row):
    # The second value in the row must be convertible to
    # a floating point number. If not then return None to
    # signify an invalid value.
    try:
        return float(row[1])
    except ValueError:
        return None


def is_row_valid(row):
    # A row is valid when:
    #   - it has exactly two values
    #   - the driver's name (first value) is not None
    #   - the driver's name (first value) is not empty
    #   - the lap time (second value) is numeric
    return row is not None and \
           len(row) == 2 and \
           row[0] is not None and \
           len(get_driver_name(row)) > 0 and \
           row[1] is not None and \
           get_lap_time(row) is not None


def ingest_row_data(row, lap_times):
    # If the driver name exists in the lap_time
    # dictionary, append to it, otherwise add it.
    driver_name = get_driver_name(row)
    lap_time = get_lap_time(row)
    if driver_name in lap_times.keys():
        driver_data = lap_times[driver_name]
        driver_data[LAP_COUNT_KEY] += 1
        driver_data[TOTAL_LAP_TIMES_KEY] += lap_time
    else:
        driver_data = {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: lap_time}
        lap_times[driver_name] = driver_data


def ingest_row(row, lap_times):
    if is_row_valid(row):
        ingest_row_data(row, lap_times)


def get_lap_data(reader):
    lap_times = {}
    for row in reader:
        ingest_row(row, lap_times)

    return lap_times


def calculate_average_lap_time(lap_times):
    # Create a new dictionary based on lap times dictionary
    # The new dictionary calculates the average lap time based
    # on the accumulated laps and total time for each driver.
    return {driver_name: driver_data[TOTAL_LAP_TIMES_KEY] / driver_data[LAP_COUNT_KEY]
            for (driver_name, driver_data)
            in lap_times.items()}


def get_top_three(average_lap_time):
    # Sort the average times in ascending order
    # given the value of the average lap time
    # return the top three (shortest lap average
    # lap time) values.
    sorted_data = sorted(average_lap_time.items(), key=operator.itemgetter(1))
    return sorted_data[:3]


def write_top_three(args, top_three):
    with open(args.output_file_pathname, 'w') as output_csv:
        writer = csv.writer(output_csv, quoting=csv.QUOTE_MINIMAL)
        for entry in top_three:
            writer.writerow(entry)


def main():
    parser = argparse.ArgumentParser(
        description='ingests driver lap times and outputs the top three drivers based on lap times')
    parser.add_argument('--input-file-pathname', help='path to CSV data file to ingest', required=True)
    parser.add_argument('--output-file-pathname', help='path to write CSV output to', required=True)
    args = parser.parse_args()

    with open(args.input_file_pathname) as input_csv:
        reader = csv.reader(input_csv, delimiter=',')
        lap_times = get_lap_data(reader)
        average_lap_time = calculate_average_lap_time(lap_times)
        top_three = get_top_three(average_lap_time)
        write_top_three(args, top_three)


if __name__ == "__main__":
    # execute only if run as a script
    main()

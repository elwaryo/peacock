#!/usr/bin/env python3.7

import unittest
from unittest import TestCase

from top_three_drivers import LAP_COUNT_KEY
from top_three_drivers import TOTAL_LAP_TIMES_KEY
from top_three_drivers import calculate_average_lap_time
from top_three_drivers import get_driver_name
from top_three_drivers import get_lap_data
from top_three_drivers import get_lap_time
from top_three_drivers import get_top_three
from top_three_drivers import ingest_row_data
from top_three_drivers import is_row_valid


class TestIs_top_three_divers(TestCase):

    def test_get_driver_name_success(self):
        self.assertEqual("Hamilton", get_driver_name(["  Hamilton\t\t"]),
                         "get_driver_name must remove leading and trailing spaces")

    def test_get_driver_name_proper_casing(self):
        self.assertEqual("Hamilton", get_driver_name(["  HamiltoN\t\t"]),
                         "get_driver_name must remove leading and trailing spaces")

    @unittest.expectedFailure
    def test_get_driver_name_Error(self):
        get_driver_name([None])

    def test_get_lap_time_int(self):
        self.assertEqual(get_lap_time(['B', '17']), 17,
                         "get_lap_time must return a floating point number"
                         "when the string input contains an integer number")

    def test_get_lap_time_float(self):
        self.assertEqual(get_lap_time(['A', '3.14159']), 3.14159,
                         "get_lap_time must return a floating point number"
                         "when the string input contains an floating point number")

    def test_get_lap_time_none_value(self):
        self.assertIsNone(get_lap_time('Hello'),
                          "get_lap_time must return a None when the string input does not contain a "
                          "value that can be converted to floating point number")

    def test_row_is_none(self):
        self.assertFalse(is_row_valid(None), "When the row is empty, it is not valid")

    def test_empty_row(self):
        self.assertFalse(is_row_valid([]), "When the row is empty, it is not valid")

    def test_too_many_values(self):
        self.assertFalse(is_row_valid([0, 1, 2]), "When the row has more than two values, it is not valid")

    def test_missing_lap_time(self):
        self.assertFalse(is_row_valid(['Hamilton']), "When the row has one value, it is not valid")

    def test_empty_driver_name(self):
        self.assertFalse(is_row_valid(['   \t\t', 'fast']), "When Driver's name is empty, it is not valid")

    def test_driver_name_is_none(self):
        self.assertFalse(is_row_valid([None, 'fast']), "When Driver's name is empty, it is not valid")

    def test_bad_lap_time(self):
        self.assertFalse(is_row_valid(['Hamilton', 'fast']), "When the row's lap time is not numeric, it is not valid")

    def test_lap_time_is_none(self):
        self.assertFalse(is_row_valid(['Hamilton', None]), "When the row's lap time is not numeric, it is not valid")

    def test_valid_row(self):
        self.assertTrue(is_row_valid([' Hamilton ', '5.8']), "When both driver's name is provided and the lap time"
                                                             "is numeric, the row is valid ")

    def test_ingest_row_data(self):
        lap_times = {}
        row = ['Alonzo', '4.32']
        ingest_row_data(row, lap_times)
        self.assertEqual({'Alonzo': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.32}},
                         lap_times)

        row = ['Verstrappen', '4.75']
        ingest_row_data(row, lap_times)
        self.assertEqual({'Alonzo': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.32},
                          'Verstrappen': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.75}},
                         lap_times)

        row = ['Hamilton', '4.65']
        ingest_row_data(row, lap_times)
        self.assertEqual({'Alonzo': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.32},
                          'Verstrappen': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.75},
                          'Hamilton': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.65}},
                         lap_times)

        row = ['Alonzo', '4.88']
        ingest_row_data(row, lap_times)
        self.assertEqual({'Alonzo': {LAP_COUNT_KEY: 2, TOTAL_LAP_TIMES_KEY: 9.2},
                          'Verstrappen': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.75},
                          'Hamilton': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.65}},
                         lap_times)

    def test_ingest_row(self):
        rows = [
            ['Alonzo', '4.32'],
            ['Verstrappen', '4.75'],
            ['Hamilton', '4.65'],
            ['Alonzo', '4.88']
        ]
        lap_times = get_lap_data(rows)

        self.assertEqual({'Alonzo': {LAP_COUNT_KEY: 2, TOTAL_LAP_TIMES_KEY: 9.2},
                          'Verstrappen': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.75},
                          'Hamilton': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.65}},
                         lap_times)

    def test_calculate_average_lap_time(self):
        lap_times = {'Alonzo': {LAP_COUNT_KEY: 2, TOTAL_LAP_TIMES_KEY: 9.2},
                     'Verstrappen': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.75},
                     'Hamilton': {LAP_COUNT_KEY: 1, TOTAL_LAP_TIMES_KEY: 4.65}}
        averages = calculate_average_lap_time(lap_times)
        self.assertEqual({'Alonzo': 4.6, 'Hamilton': 4.65, 'Verstrappen': 4.75},
                         averages)

    def test_get_top_three(self):
        lap_times = {'A': 11, 'B': 5, 'C': 17, 'D': 1, 'E': 3}
        top_three = get_top_three(lap_times)
        self.assertEqual(top_three, [('D', 1), ('E', 3), ('B', 5)])


if __name__ == '__main__':
    unittest.main()
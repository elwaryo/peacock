# Unattended Test for Data Engineer

You are going to build a simple ETL which will process F1 drivers’ lap times to determine the
average value for each drive and orders them to return the top three positions in ascending
order

## EXTRACT
This is a batch process. Your pipeline will ingest a single file which contains data in a CVS
format. The data consists of lap times for 5 drivers. The input data is as follows
Alonzo,4.32
Verstrappen,4.75
Alonzo,4.88
Hamilton,4.65
Alonzo,4.38
Verstrappen,4.55
Hamilton,4.61
Hamilton,4.43
Verstrappen,4.59

## TRANFORM
Calculate the average lap time for each driver and sort them in ascending order

## LOAD
The output should be the top 3 drivers with the lowest average lap time in ascending order.
You are free to choose the format of the output and how to store. For example you can
create a new file with the output in CVS format.
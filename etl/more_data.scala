import org.apache.spark.sql.SaveMode

val driverDF = spark.read.option("inferSchema", "true").
    option("header", "true").
    csv("data/additional/datasets_5571_8322_drivers.csv").
    select("driverId", "driverRef")

val lapTimesDf = spark.read.option("inferSchema", "true").
    option("header", "true").
    csv("data/additional/lapTimes.csv").
    select("driverId", "milliseconds")

val driverLapTime = lapTimesDf.as("lapTime").join(driverDF.as("driver"), $"lapTime.driverId" ===  $"driver.driverId", "inner").
    select("driverRef", "milliseconds").
    withColumn("lapTime", expr("milliseconds/60000.0")).
    select("driverRef", "lapTime")

driverLapTime.repartition(1).
    write.mode(SaveMode.Overwrite).
    format("csv").
    save("data/additional/more_lap_times")